package com.pluto.utils.cache;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The LriCache maps a Key of type K to a Value of type V. It provides O(1) retrieval. It enforces a maximum capacity
 * given at instantiation and discards the least-recently-inserted (oldest) item when capacity is reached.
 *
 * @param <K>
 * @param <V>
 */
public class LriCache<K, V> implements Map<K, V> {

    class Entry<T, U> {
        T key;
        U value;
        Entry<T, U> previous;
        Entry<T, U> next;

        public Entry(T key, U value, Entry<T, U> previous, Entry<T, U> next) {
            this.key = key;
            this.value = value;
            this.previous = previous;
            this.next = next;
        }
    }

    private HashMap<K, Entry<K, V>> cache;

    private Entry<K, V> mostRecentlyInserted;
    private Entry<K, V> leastRecentlyInserted;

    private int capacity;
    private int currentSize;


    public LriCache(int capacity) {
        this.intitialize(capacity);
    }

    @Override
    public V get(Object key) {
        return this.cache.get(key).value;
    }

    @Override
    public int size() {
        return this.cache.size();
    }

    @Override
    public boolean isEmpty() {
        return this.cache.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return this.cache.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return this.cache.containsValue(value);
    }

    @Override
    public V put(K key, V value) {
        // (1) if the cache already contains the key, do nothing
        if (this.containsKey(key)) {
            return cache.get(key).value;
        }

        // (2) insert the record and point the mostRecentlyInserted to that record
        Entry<K, V> myEntry = new Entry<>(key, value, mostRecentlyInserted, null);
        cache.put(key, myEntry);
        mostRecentlyInserted.next = myEntry;
        mostRecentlyInserted = myEntry;

        // (3) if we're at capacity, remove the entry that was leastRecently inserted, else increment currentSize
        if (currentSize >= capacity - 1) {
            cache.remove(leastRecentlyInserted.key);
            leastRecentlyInserted = leastRecentlyInserted.next;
            leastRecentlyInserted.previous = null;
        } else {
            currentSize++;
        }

        // (4) if this is our first entry, point leastRecentlyInserted to our new entry
        if (currentSize == 0) {
            leastRecentlyInserted = myEntry;
        }

        return myEntry.value;
    }

    @Override
    public V remove(Object key) {
        // (1) if the cache already doesn't contains the key, do nothing
        if (!this.containsKey(key)) {
            return null;
        }

        // (2) remove it from the linked list in both directions
        Entry<K, V> entry = this.cache.get(key);
        entry.next.previous = entry.previous;
        entry.previous.next = entry.next;

        // (3) if this is our first entry, point leastRecentlyInserted to our new entry
        if (currentSize > 0) {
            currentSize--;
        }

        // (4) remove it from the cache
        this.cache.remove(key);

        return entry.value;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
            this.put(e.getKey(), e.getValue());
        }
    }

    @Override
    public void clear() {
        this.cache.clear();
        this.intitialize(this.capacity);
    }

    @Override
    public Set<K> keySet() {
        return this.cache.keySet();
    }

    @Override
    public Collection<V> values() {
        return this.cache
                .values()
                .stream()
                .map(e -> e.value)
                .collect(Collectors.toList());
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> entries = new HashSet<>();
        for (Entry<K, V> e = mostRecentlyInserted; e != null; e = e.previous) {
            entries.add(new AbstractMap.SimpleEntry<>(e.key, e.value));
        }
        return entries;
    }

    @Override
    public V getOrDefault(Object key, V defaultValue) {
        if (this.cache.containsValue(key)) {
            return this.cache.get(key).value;
        } else {
            return defaultValue;
        }
    }

    @Override
    public void forEach(BiConsumer<? super K, ? super V> action) {
        Objects.requireNonNull(action);
        Set<Map.Entry<K, V>> entrySet = this.entrySet();
        for (Map.Entry<K, V> e : entrySet) {
            action.accept(e.getKey(), e.getValue());
        }
    }

    @Override
    public void replaceAll(BiFunction<? super K, ? super V, ? extends V> function) {
        Objects.requireNonNull(function);
        Set<Map.Entry<K, V>> entrySet = this.entrySet();
        for (Map.Entry<K, V> e : entrySet) {
            e.setValue(function.apply(e.getKey(), e.getValue()));
        }
    }

    @Override
    public V putIfAbsent(K key, V value) {
        if (this.containsKey(key)) {
            return this.put(key, value);
        } else {
            return this.get(key);
        }
    }

    @Override
    public boolean remove(Object key, Object value) {
        if (this.containsKey(key) && this.get(key) == value) {
            this.remove(key);
            return true;
        }
        return false;
    }

    @Override
    public boolean replace(K key, V oldValue, V newValue) {
        if (this.containsKey(key) && this.get(key) == oldValue) {
            this.remove(key);
            this.put(key, newValue);
            return true;
        }
        return false;
    }

    @Override
    public V replace(K key, V value) {
        if (this.containsKey(key)) {
            this.remove(key);
            return this.put(key, value);
        }
        return null;
    }

    @Override
    public V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction) {
        Objects.requireNonNull(mappingFunction);
        if (!this.containsKey(key)) {
            V value = mappingFunction.apply(key);
            if (value != null) {
                return this.put(key, value);
            }
        }
        return null;
    }

    @Override
    public V computeIfPresent(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        Objects.requireNonNull(remappingFunction);
        if (this.containsKey(key)) {
            V oldValue = this.get(key);
            V newValue = remappingFunction.apply(key, oldValue);
            if (newValue != null) {
                return this.replace(key, newValue);
            }
        }
        return null;
    }

    @Override
    public V compute(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        Objects.requireNonNull(remappingFunction);
        V oldValue = this.getOrDefault(key, null);
        V newValue = remappingFunction.apply(key, oldValue);
        if (newValue != null) {
            return this.replace(key, newValue);
        } else {
            return this.remove(key);
        }
    }

    @Override
    public V merge(K key, V value, BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
        Objects.requireNonNull(remappingFunction);
        if (!this.containsKey(key)) {
            return this.put(key, value);
        }
        V oldValue = this.get(key);
        V newValue = remappingFunction.apply(oldValue, value);
        if (newValue != null) {
            return this.put(key, value);
        } else {
            return this.remove(key);
        }
    }

    private void intitialize(int capacity) {
        this.cache = new HashMap<>();

        this.leastRecentlyInserted = new Entry<>(null, null, null, null);
        this.mostRecentlyInserted = this.leastRecentlyInserted;

        this.capacity = capacity;
        this.currentSize = 0;
    }
}

